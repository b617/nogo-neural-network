#include "NetworkGameClient.h"
#include <iostream>

namespace NoGo
{

	NetworkGameClient::NetworkGameClient(IPlayer* player) : NoGoGame(), localPlayer(player)
	{
	}

	NetworkGameClient::~NetworkGameClient()
	{
		if (server != nullptr)
		{
			delete server;
		}
	}


	NoGo::MoveResult NetworkGameClient::Update()
	{
		IPlayer* currentPlayer = GetCurrentPlayer();
		try
		{
			NoGo::uint move;


			move = currentPlayer->MakeMove(board);

			if (currentPlayer == localPlayer)
			{
				SendMove(move);
			}
			MakeMove(move);
		}
		catch (const ServerMessage& message)
		{
			switch (message)
			{
			case ServerMessage::Win:
			case ServerMessage::WinDisconnect:
			case ServerMessage::WinTimeout:
				return (localPlayer == playerBlack) ? MoveResult::BLACK_WON : MoveResult::WHITE_WON;
			case ServerMessage::Lost:
			case ServerMessage::LostTimeout:
				return (localPlayer == playerBlack) ? MoveResult::WHITE_WON : MoveResult::BLACK_WON;
			default:
				puts("Uncached exception in NetworkGameClient::Update(): ");
				puts(MessageToString(message).c_str());
				puts("\n");
			}
		}

		return MoveResult::GAME_CONTINUES;
	}


	bool NetworkGameClient::Connect(const char* serverIP, const char* serverPort, const char* playerName)
	{
		server = new tcp::iostream (serverIP, serverPort);
		if (server == nullptr || !(*server))
		{
			if (server != nullptr)
			{
				delete server;
			}
			server = nullptr;
			return false;
		}
		remotePlayer.server = server;
		SendToServer(ServerMessage::Connect, playerName, server);
		return true;
	}

	bool NetworkGameClient::AwaitGameStart()
	{
		std::string args;

		ServerMessage m = ListenForServer(args, server);
		if (m != ServerMessage::NewGame)
		{
			puts("Error: New game didn't start. Message received:\n");
			puts((MessageToString(m) + ' ' + args).c_str());
			return false;
		}
		
		SetupNewGame(args);

		return true;
	}

	void NetworkGameClient::SetupNewGame(const std::string& dataFromServer)
	{

		std::string color = dataFromServer.substr(0, 5);
		if (color.compare("black") == 0)
		{
			playerBlack = localPlayer;
			playerWhite = &remotePlayer;
		}
		else
		{
			playerBlack = &remotePlayer;
			playerWhite = localPlayer;
		}


		SetCurrentGame(playerBlack, Field::BLACK);
		SetCurrentGame(playerWhite, Field::WHITE);

		//assuming board is 9x9, as this is the only scenario this algorithm will work on anyway.
		board.Reset();
		playerBlack->Reset();
		playerWhite->Reset();

		currentTurn = Field::BLACK;
	}

	void NetworkGameClient::SendMove(NoGo::uint move)
	{
		std::string args = "";

		Coordinate moveXY = Coordinate::FromFieldIndex(move);
		args += std::to_string(moveXY.x + 1);
		args += ' ';
		args += std::to_string(moveXY.y + 1);

		SendToServer(ServerMessage::MoveYours, args, server);
	}


	void NetworkGameClient::PlayTurney()
	{
		std::string dataFromServer;
		ServerMessage messageFromServer;
		MoveResult moveResult;

		std::cout << "Awaiting tourney start...\n";

		while ((messageFromServer = ListenForServer(dataFromServer, server)) == ServerMessage::NewGame)
		{
			SetupNewGame(dataFromServer);
			moveResult = MoveResult::GAME_CONTINUES;

			while (moveResult == MoveResult::GAME_CONTINUES)
			{
				moveResult = Update();
			}
			
			if ((moveResult == MoveResult::BLACK_WON && localPlayer == playerBlack) || (moveResult == MoveResult::WHITE_WON && localPlayer == playerWhite))
			{
				std::cout << "You won! ^^\n";
			}
			else
			{
				std::cout << "You lost. :(\n";
			}

			std::cout << "Awaiting next game...\n";
		}

		if (messageFromServer == ServerMessage::TurneyEnd)
		{
			std::cout << "=== Tourney end ===\nYour position: " << dataFromServer;
		}
		else
		{
			std::cout << "Uncached exception in NetworkGameClient::PlayTurney(). Received message from server:\n"
				<< MessageToString(messageFromServer) << ' ' << dataFromServer << std::endl << 
				"While expecting NewGame code (200) or TurneyEnd code (299).\n";
		}
	}

}
