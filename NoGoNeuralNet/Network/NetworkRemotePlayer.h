#pragma once
#include "..\Player\IPlayer.h"
#include <boost\asio.hpp>

using boost::asio::ip::tcp;


class NetworkRemotePlayer : public NoGo::IPlayer
{
public:
	NetworkRemotePlayer();
	~NetworkRemotePlayer();

	virtual NoGo::uint MakeMove(const NoGo::Board& currentBoardState) override;


	virtual IPlayer* Clone() override;


public:

	tcp::iostream* server = nullptr;
};

