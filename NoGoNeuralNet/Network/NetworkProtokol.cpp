#include "NetworkProtokol.h"
#include <iostream>

//#define DEBUG_NETWORKING

std::string MessageToString(ServerMessage command)
{
	switch (command)
	{
	case ServerMessage::Connect:
		return "100";
	case ServerMessage::NewGame:
		return "200";
	case ServerMessage::MoveYours:
		return "210";
	case ServerMessage::MoveOpponent:
		return "220";
	case ServerMessage::Win:
		return "230";
	case ServerMessage::WinTimeout:
		return "231";
	case ServerMessage::WinDisconnect:
		return "232";
	case ServerMessage::Lost:
		return "240";
	case ServerMessage::LostTimeout:
		return "241";
	case ServerMessage::TurneyEnd:
		return "299";
	case ServerMessage::Error:
		return "999";
	default:
		return "000";
	}
}

ServerMessage StringToMessage(std::string command)
{
	if (0 == command.compare("100"))
	{
		return ServerMessage::Connect;
	}
	else if (0 == command.compare("200"))
	{
		return ServerMessage::NewGame;
	}
	else if (0 == command.compare("210"))
	{
		return ServerMessage::MoveYours;
	}
	else if (0 == command.compare("220"))
	{
		return ServerMessage::MoveOpponent;
	}
	else if (0 == command.compare("230"))
	{
		return ServerMessage::Win;
	}
	else if (0 == command.compare("231"))
	{
		return ServerMessage::WinTimeout;
	}
	else if (0 == command.compare("232"))
	{
		return ServerMessage::WinDisconnect;
	}
	else if (0 == command.compare("240"))
	{
		return ServerMessage::Lost;
	}
	else if (0 == command.compare("241"))
	{
		return ServerMessage::LostTimeout;
	}
	else if (0 == command.compare("299"))
	{
		return ServerMessage::TurneyEnd;
	}
	else //if (0 == command.compare("999"))
	{
		return ServerMessage::Error;
	}
}



void SendToServer(const ServerMessage message, std::string args, tcp::iostream* server)
{
	(*server) << MessageToString(message) + ' ' + args + "\r\n";

#ifdef DEBUG_NETWORKING
	std::cout << ">>" << MessageToString(message) << ' ' << args << "\r\n";
#endif // DEBUG_NETWORKING
}

ServerMessage ListenForServer(std::string& outArgs, tcp::iostream* server)
{
	std::string buffor;
	std::getline(*server, buffor);
	size_t index = buffor.find(' ');

	ServerMessage message = StringToMessage(buffor.substr(0, index));
	outArgs = buffor.substr(index + 1);

#ifdef DEBUG_NETWORKING
	std::cout << "<<" << buffor << std::endl;
#endif // DEBUG_NETWORKING

	return message;
}
