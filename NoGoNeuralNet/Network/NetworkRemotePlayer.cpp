#include "NetworkRemotePlayer.h"
#include "NetworkProtokol.h"



NetworkRemotePlayer::NetworkRemotePlayer()
{
}


NetworkRemotePlayer::~NetworkRemotePlayer()
{
}

NoGo::uint NetworkRemotePlayer::MakeMove(const NoGo::Board& currentBoardState)
{
	std::string args;
	ServerMessage result = ListenForServer(args, server);

	if (result != ServerMessage::MoveOpponent)
	{
		throw result;
	}

	std::istringstream data(args);
	unsigned int x, y;
	data >> x >> y;

	return NoGo::Coordinate(x - 1, y - 1);
}

NoGo::IPlayer* NetworkRemotePlayer::Clone()
{
	NetworkRemotePlayer* newPlayer = new NetworkRemotePlayer();
	newPlayer->server = server;
	return newPlayer;
}
