#pragma once
#include "..\Game\NoGoGame.h"
#include "NetworkRemotePlayer.h"
#include <boost\asio.hpp>
#include <string>
#include "NetworkProtokol.h"

using boost::asio::ip::tcp;

namespace NoGo
{

	class NetworkGameClient : public NoGoGame
	{
	public:
		NetworkGameClient(IPlayer* player);
		~NetworkGameClient();

		virtual MoveResult Update() override;


		bool Connect(const char* serverIP, const char* serverPort, const char* playerName = "Daniel_Janowski");
		bool AwaitGameStart();
		void SetupNewGame(const std::string& dataFromServer);
		void SendMove(NoGo::uint move);
		void PlayTurney();

	public:
		IPlayer* localPlayer;
		NetworkRemotePlayer remotePlayer;
		tcp::iostream* server = nullptr;
	};

}

