#pragma once
#include <string.h>
#include <boost/asio.hpp>

using boost::asio::ip::tcp;

enum class ServerMessage
{
	Connect,
	MoveYours,
	MoveOpponent,
	NewGame,
	Win,
	WinTimeout,
	WinDisconnect,
	Lost,
	LostTimeout,
	TurneyEnd,
	Error
};

std::string MessageToString(ServerMessage command);
ServerMessage StringToMessage(std::string command);


void SendToServer(const ServerMessage message, std::string args, tcp::iostream* server);
ServerMessage ListenForServer(std::string& outArgs, tcp::iostream* server);
