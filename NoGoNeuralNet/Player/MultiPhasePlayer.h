#pragma once
#include "IPlayer.h"
class MultiPhasePlayer : public NoGo::IPlayer
{
public:
	MultiPhasePlayer();
	~MultiPhasePlayer();

	virtual NoGo::uint MakeMove(const NoGo::Board& currentBoardState) override;


	virtual NoGo::IPlayer* Clone() override;


	virtual void Reset() override;

public:
	size_t moveCounter = 0;
};

