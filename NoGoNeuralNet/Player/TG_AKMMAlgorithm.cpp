#include "TG_AKMMAlgorithm.h"

#define COORDS(X,Y) NoGo::Coordinate(X,Y).ToFieldIndex()

TG_AKMMAlgorithm::TG_AKMMAlgorithm()
{
	SetupValuesMap();

	for (int x = 0; x < 9; x++)
	{
		for (int y = 0; y < 9; y++)
		{
			emptyPoints.push_back(NoGo::Coordinate(x, y));
		}
	}
}


TG_AKMMAlgorithm::~TG_AKMMAlgorithm()
{
}

NoGo::uint TG_AKMMAlgorithm::MakeMove(const NoGo::Board& currentBoardState)
{
	using namespace NoGo;

	//W.I.P.
	return -1;
}

NoGo::IPlayer* TG_AKMMAlgorithm::Clone()
{
	return new TG_AKMMAlgorithm();
}

void TG_AKMMAlgorithm::SetupValuesMap()
{
	for (int y = 0; y < NoGo::Board::SIZE_Y; y++)
		for (int x = 0; x < NoGo::Board::SIZE_X; x++)
		{
			valuesMap[COORDS(x, y)] = 0;
			if (x == 0 || x == NoGo::Board::BOARD_SIZE - 1)
			{
				valuesMap[COORDS(x, y)] += 2;
			}
			if (y == 0 || y == NoGo::Board::BOARD_SIZE - 1)
			{
				valuesMap[COORDS(x, y)] += 2;
			}
		}
}

#undef COORDS