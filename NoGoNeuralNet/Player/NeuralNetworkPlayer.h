#pragma once
#include "IPlayer.h"
#include "../Neural/NeuralNetwork.h"


class NeuralNetworkPlayer : public NoGo::IPlayer
{
public:
	NeuralNetworkPlayer(Neural::BasicNeuralNetwork* network);
	~NeuralNetworkPlayer();

	virtual NoGo::uint MakeMove(const NoGo::Board& currentBoardState) override;

	/**
	* @brief Sets new neural network for this player to use
	*
	* @param network - new neural network to use
	* @return previous neural network (you should handle deleting it yourself)
	*/
	Neural::BasicNeuralNetwork* SetNewNeuralNetwork(Neural::BasicNeuralNetwork* network);


	virtual IPlayer* Clone() override;

protected:
	/**
	* @brief Transforms current board state into neural network inputs
	*
	* @param currentBoard - current board state
	* @return (float[]) array of inputs for neural network
	*/
	float* GenerateInputs(const NoGo::Board& currentBoard);
	/**
	* @brief Transforms output from neural network into a move
	*
	* Sorts outputs array and takes first of them that is a valid move
	* @param outputs - outputs from neural network
	* @return char - move
	*/
	NoGo::uint ProcessOutputs(const float* outputs);
	/**
	* @brief Transforms a single board piece into a single piece of neural network input
	*
	* Allows the same network to play as X-s and O-s - it hides symbols behind line of abstraction
	* "Your symbol", "nothing" and "opponent's symbol"
	* @param field a single board piece: (Empty/X/O)
	* @return float representation of said field that is invariant to what color (X/O) is network playing as
	*/
	inline float FieldToFloat(const NoGo::Field& field);

private:
	//Neural network used by this player
	Neural::BasicNeuralNetwork* network;
};

