#include "IPlayer.h"
#include <exception>
#include "../Game/NoGoGame.h"

namespace NoGo
{

	IPlayer::IPlayer()
	{
	}


	IPlayer::~IPlayer()
	{
	}

	Field IPlayer::GetColor()
	{
		return color;
	}

	void IPlayer::Reset()
	{
	}

	bool IPlayer::IsValidMove(uint move)
	{
		return currentGame->IsValidMove(move);
	}

	bool IPlayer::IsValidMove(const Board& board, const uint move, const Field moveColor)
	{
		return currentGame->IsValidMove(board, move, moveColor);
	}

}
