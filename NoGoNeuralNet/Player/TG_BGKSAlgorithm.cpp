#include "TG_BGKSAlgorithm.h"
#include "../Game/Group.h"

#define COORDS(X,Y) (NoGo::Coordinate(X,Y).ToFieldIndex())

TG_BGKSAlgorithm::TG_BGKSAlgorithm()
{
	for (size_t i = 0; i < NoGo::Board::SIZE_X; i++)
	{
		for (size_t j = 0; j < NoGo::Board::SIZE_Y; j++)
		{
			pustePola.push_back(NoGo::Coordinate(i, j));
		}
	}
}


TG_BGKSAlgorithm::~TG_BGKSAlgorithm()
{
	if (liczbaWygranychRuchu != nullptr)
	{
		delete[] liczbaWygranychRuchu;
	}
}

NoGo::uint TG_BGKSAlgorithm::MakeMove(const NoGo::Board& currentBoardState)
{
	moveEndTime = std::chrono::steady_clock::now() + std::chrono::seconds(1);
	using namespace NoGo;
	plansza = currentBoardState;

	if (GetColor() == Field::WHITE)
	{
		kolorPrzeciwnika = Field::BLACK;
		naszKolor = Field::WHITE;
	}
	else
	{
		kolorPrzeciwnika = Field::WHITE;
		naszKolor = Field::BLACK;
	}

	for (int i = pustePola.size() - 1; i >= 0; i--)
	{
		if (currentBoardState.Get(pustePola[i]) != Field::EMPTY)
		{
			pustePola.erase(pustePola.begin() + i);
			break;
		}
	}

	mozliweRuchy.clear();

	for (Coordinate pole : pustePola)
	{
		if (IsValidMove(pole.ToFieldIndex()))
		{
			mozliweRuchy.push_back(pole);
		}
	}

	if (corners)
	{
		if (Contains(mozliweRuchy, cornerA))
		{
			RemoveValues(pustePola, cornerA);
			return cornerA;
		}
		else if (Contains(mozliweRuchy, cornerB))
		{
			RemoveValues(pustePola, cornerB);
			return cornerB;
		}
		else if (Contains(mozliweRuchy, cornerC))
		{
			RemoveValues(pustePola, cornerC);
			return cornerC;
		}
		else if (Contains(mozliweRuchy, cornerD))
		{
			corners = false;
			RemoveValues(pustePola, cornerD);
			return cornerD;
		}
	}




	liczbaMozliwychRuchow = mozliweRuchy.size();
	if (liczbaWygranychRuchu != nullptr) delete[] liczbaWygranychRuchu;
	liczbaWygranychRuchu_size = liczbaMozliwychRuchow;
	liczbaWygranychRuchu = new int[liczbaMozliwychRuchow];
	ResetWygranych();

	Coordinate najlepszyRuch;

	do
	{
		for (int ruch = 0; ruch < liczbaMozliwychRuchow; ruch++)
		{
			//symulacja dla kazdego punktu
			//Log.d("Symulacja", ruch + "");
			Symulacja(ruch);
			if (GetRemainingTimeMS() <= 0) break;
		}
	} while (GetRemainingTimeMS() >= 0);

	//wybor najlepszego punktu
	najlepszyRuch = WyborZNajlepszych();

	RemoveValues(pustePola, najlepszyRuch);
	return najlepszyRuch;
}

NoGo::IPlayer* TG_BGKSAlgorithm::Clone()
{
	return new TG_BGKSAlgorithm();
}

void TG_BGKSAlgorithm::Symulacja(NoGo::uint ruch)
{
	using namespace NoGo;

	Board subPlansza(plansza);
	Field tempC = kolorPrzeciwnika;

	Coordinate punkt = mozliweRuchy[ruch];


	subPlansza.MakeMove(punkt, GetColor());

	std::vector<uint> simMozliweRuchy = DostepneRuchy(subPlansza, tempC);

	bool sim = true;

	while (sim)
	{
		if (simMozliweRuchy.size() != 0)
		{
			int nastepnyRuch = rand() % (simMozliweRuchy.size());
			subPlansza.MakeMove(simMozliweRuchy[nastepnyRuch], tempC);

			if (tempC == naszKolor)
				tempC = kolorPrzeciwnika;
			else
				tempC = naszKolor;

			simMozliweRuchy = DostepneRuchy(subPlansza, tempC);

		}
		else
		{
			if (tempC == kolorPrzeciwnika)
			{
				liczbaWygranychRuchu[ruch] += 1;
			}
			sim = false;
		}

		if (GetRemainingTimeMS() <= 0)
			break;

	}
}

NoGo::Coordinate TG_BGKSAlgorithm::WyborZNajlepszych()
{
	int max = liczbaWygranychRuchu[0];
	int punkt;
	NoGo::Coordinate point;

	if (liczbaWygranychRuchu_size <= 0)
	{
		if (mozliweRuchy.size() > 0)
			return mozliweRuchy[0];
		else
			return cornerA;
	}

	for (size_t i = 0; i < liczbaWygranychRuchu_size; i++)
		if (liczbaWygranychRuchu[i] > max) max = liczbaWygranychRuchu[i];

	for (size_t i = 0; i < liczbaWygranychRuchu_size; i++)
		if (liczbaWygranychRuchu[i] == max) najlepszeRuchy.push_back(i);
	
	punkt = najlepszeRuchy[rand() % najlepszeRuchy.size()];
	point = mozliweRuchy[punkt];

	return point;
}

void TG_BGKSAlgorithm::ResetWygranych()
{
	for (size_t i = 0; i < liczbaWygranychRuchu_size; i++)
	{
		liczbaWygranychRuchu[i] = 0;
	}

	najlepszeRuchy.clear();
}

bool TG_BGKSAlgorithm::SprawdzRuch(NoGo::Coordinate punkt, NoGo::Board& subPlansza, NoGo::Field kolor)
{
	using namespace NoGo;

	if (!(punkt.x < subPlansza.SIZE_X && punkt.y < subPlansza.SIZE_Y && punkt.x >= 0 && punkt.y >= 0))
		return false;

	if (subPlansza.Get(punkt) != Field::EMPTY)
		return false;

	Board subSubPlansza(subPlansza);
	subSubPlansza.MakeMove(punkt, kolor);
	if (!Group::DoAllGroupsHaveLiberties(subSubPlansza))
	{
		return false;
	}

	return true;
}

std::vector<NoGo::uint> TG_BGKSAlgorithm::DostepneRuchy(NoGo::Board& subPlansza, NoGo::Field kolor)
{
	std::vector<NoGo::uint> ruchy;

	for (int i = 0; i < subPlansza.SIZE_X; i++)
	{
		for (int j = 0; j < subPlansza.SIZE_Y; j++)
		{
			if (SprawdzRuch(NoGo::Coordinate(i, j), subPlansza, kolor))
				ruchy.push_back(COORDS(i, j));
		}
	}

	return  ruchy;
}

long long TG_BGKSAlgorithm::GetRemainingTimeMS()
{
	const auto currentTime = std::chrono::steady_clock::now();
	return std::chrono::duration_cast<std::chrono::milliseconds>(moveEndTime - currentTime).count();
}

#undef COORDS
