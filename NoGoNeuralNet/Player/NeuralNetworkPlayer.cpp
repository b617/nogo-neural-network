#include "NeuralNetworkPlayer.h"
#include "../Utility/Algorithms.h"


NeuralNetworkPlayer::NeuralNetworkPlayer(Neural::BasicNeuralNetwork* network) : network(network)
{ }
NeuralNetworkPlayer::~NeuralNetworkPlayer()
{ }

NoGo::uint NeuralNetworkPlayer::MakeMove(const NoGo::Board& currentBoardState)
{
	float* inputs = GenerateInputs(currentBoardState);
	size_t unusedTemp;
	const float* outputs = network->Compute(inputs, NoGo::Board::BOARD_SIZE, unusedTemp);
	NoGo::uint move = ProcessOutputs(outputs);
	delete[] inputs;
	return move;
}

Neural::BasicNeuralNetwork* NeuralNetworkPlayer::SetNewNeuralNetwork(Neural::BasicNeuralNetwork * network)
{
	Neural::BasicNeuralNetwork* previous = this->network;
	this->network = network;
	return previous;
}

NoGo::IPlayer* NeuralNetworkPlayer::Clone()
{
	return new NeuralNetworkPlayer(network);
}

float* NeuralNetworkPlayer::GenerateInputs(const NoGo::Board& currentBoard)
{
	float* input = new float[NoGo::Board::BOARD_SIZE];
	for (char i = 0; i < NoGo::Board::BOARD_SIZE; i++)
	{
		input[i] = FieldToFloat(currentBoard.Get(i));
	}
	return input;
}

NoGo::uint NeuralNetworkPlayer::ProcessOutputs(const float* outputs)
{
	std::vector<float> _outputs(outputs, outputs + NoGo::Board::BOARD_SIZE);
	std::vector<size_t>& sortedIndexes = sort_indexes(_outputs);
	for (size_t i = 0; i < NoGo::Board::BOARD_SIZE; i++)
	{
		if (IsValidMove((NoGo::uint)sortedIndexes[i]))
		{
			return (NoGo::uint)sortedIndexes[i];
		}
	}
	return (NoGo::uint)0;
}

inline float NeuralNetworkPlayer::FieldToFloat(const NoGo::Field& field)
{
	if (field == NoGo::Field::EMPTY) return 0.0f;
	if (field == GetColor()) return 1.0f;
	else return -1.0f;
}