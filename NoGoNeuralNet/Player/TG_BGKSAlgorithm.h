#pragma once
#include "IPlayer.h"
#include <chrono>
#include <algorithm>

class TG_BGKSAlgorithm : public NoGo::IPlayer
{
public:
	TG_BGKSAlgorithm();
	~TG_BGKSAlgorithm();

	virtual NoGo::uint MakeMove(const NoGo::Board& currentBoardState) override;
	virtual NoGo::IPlayer* Clone() override;

private:
	void Symulacja(NoGo::uint ruch);
	NoGo::Coordinate WyborZNajlepszych();
	void ResetWygranych();
	bool SprawdzRuch(NoGo::Coordinate punkt, NoGo::Board& subPlansza, NoGo::Field kolor);
	std::vector<NoGo::uint> DostepneRuchy(NoGo::Board& subPlansza, NoGo::Field kolor);


	NoGo::Board plansza;
	std::vector<NoGo::Coordinate> mozliweRuchy;
	int liczbaMozliwychRuchow;
	int* liczbaWygranychRuchu = nullptr;
	size_t liczbaWygranychRuchu_size = 0;
	std::vector<int> najlepszeRuchy;
	std::vector<NoGo::Coordinate> pustePola;

	NoGo::Field naszKolor, kolorPrzeciwnika;

	bool corners;
	const NoGo::Coordinate cornerA = NoGo::Coordinate(0, 0);
	const NoGo::Coordinate cornerB = NoGo::Coordinate(0, 8);
	const NoGo::Coordinate cornerC = NoGo::Coordinate(8, 8);
	const NoGo::Coordinate cornerD = NoGo::Coordinate(8, 0);


	std::chrono::time_point<std::chrono::steady_clock> moveEndTime;

	long long GetRemainingTimeMS();

	template<typename T>
	bool Contains(const std::vector<T>& vector, const T& element)
	{
		return std::find(vector.begin(), vector.end(), element) != vector.end();
	}
	template<typename T>
	inline void RemoveValues(std::vector<T>& vector, const T& element)
	{
		vector.erase(std::remove(vector.begin(), vector.end(), element), vector.end());
	}
};

