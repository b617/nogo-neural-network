#include "TG_WBMBAlgorithm.h"


TG_WBMBAlgorithm::TG_WBMBAlgorithm()
{
}
TG_WBMBAlgorithm::~TG_WBMBAlgorithm()
{
}

NoGo::IPlayer* TG_WBMBAlgorithm::Clone()
{
	return new TG_WBMBAlgorithm();
}


std::vector<NoGo::Coordinate> TG_WBMBAlgorithm::GetAllValidMoves(const NoGo::Board& board, NoGo::Field color)
{
	std::vector<NoGo::Coordinate> theList;

	for (NoGo::uint i = 0; i < NoGo::Board::BOARD_SIZE; i++)
	{
		if (IsValidMove(board, i, color))
		{
			theList.push_back(NoGo::Coordinate::FromFieldIndex(i));
		}
	}

	return theList;
}

long long TG_WBMBAlgorithm::GetRemainingTimeMS()
{
	const auto currentTime = std::chrono::steady_clock::now();
	return std::chrono::duration_cast<std::chrono::milliseconds>(moveEndTime - currentTime).count();
}

NoGo::uint TG_WBMBAlgorithm::MakeMove(const NoGo::Board& startBoardState)
{
	moveEndTime = std::chrono::steady_clock::now() + std::chrono::seconds(1);

	std::vector<NoGo::Coordinate> startValidMoves = GetAllValidMoves(startBoardState, GetColor());
	std::vector<int> startValidMovesWins, startValidMovesSimCounter;
	std::vector<NoGo::Coordinate> tempValidMoves(startValidMoves);

	NoGo::Board tempBoardState(startBoardState);
	NoGo::Field myColor = GetColor();
	NoGo::Field enemyColor = OppositeColor(myColor);
	NoGo::Field actualColor = myColor;

	bool gameSimulationEnded = false;

	for (size_t k = 0; k < startValidMoves.size(); k++)
	{
		startValidMovesWins.push_back(0);
		startValidMovesSimCounter.push_back(0);
	}

	bool simulation = true;

	while (simulation)
	{
		for (size_t i = 0; i < startValidMoves.size(); i++) {
			tempBoardState.MakeMove(NoGo::Coordinate(tempValidMoves[i].x, tempValidMoves[i].y), actualColor);
			actualColor = OppositeColor(actualColor);
			tempValidMoves = GetAllValidMoves(tempBoardState, actualColor);


			while (!gameSimulationEnded) {
				if (tempValidMoves.size() != 0) {
					int nextMove = rand()%tempValidMoves.size();
					tempBoardState.MakeMove(NoGo::Coordinate(tempValidMoves[nextMove].x, tempValidMoves[nextMove].y), actualColor);
					actualColor = OppositeColor(actualColor);
					tempValidMoves = GetAllValidMoves(tempBoardState, actualColor);
				}
				else {
					if (actualColor == enemyColor) {
						++startValidMovesWins[i];
					}
					++startValidMovesSimCounter[i];
					gameSimulationEnded = true;
				}

				if (GetRemainingTimeMS() < 0) break;
			}
			tempValidMoves.clear();
			tempBoardState = startBoardState;
			gameSimulationEnded = false;
			actualColor = myColor;
			if (GetRemainingTimeMS() < 0) break;
		}
		if (GetRemainingTimeMS() < 0) break;

		//wyb�r najlepszego wyniku
		int bestMove = rand() % startValidMoves.size();
		float bestPercent = 0;

		for (size_t j = 0; j < startValidMovesWins.size(); j++)
		{
			if (startValidMovesSimCounter[j] != 0)
			{
				if ((startValidMovesWins[j] / startValidMovesSimCounter[j]) > bestPercent)
				{
					bestMove = j;
					bestPercent = (float)startValidMovesWins[j] / (float)startValidMovesSimCounter[j];
				}
			}
		}

		return startValidMoves[bestMove].ToFieldIndex();
	}

}

