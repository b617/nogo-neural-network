#pragma once
#include "../Game/Board.h"

namespace NoGo
{

	class IPlayer
	{
	private:
		friend class NoGoGame;
		Field color;
		class NoGoGame* currentGame;
	public:
		IPlayer();
		~IPlayer();

		Field GetColor();
		virtual uint MakeMove(const Board& currentBoardState) = 0;
		virtual IPlayer* Clone() = 0;
		virtual void Reset();
	protected:
		bool IsValidMove(const uint move);
		bool IsValidMove(const Board& board, const uint move, const Field moveColor);
	};

}

