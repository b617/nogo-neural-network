#pragma once
#include "IPlayer.h"
#include <vector>
#include <chrono>


/**
* Source:
* https://github.com/lukaszlipski/NoGo/blob/master/app/src/main/java/com/lucek/androidgameengine2d/controllers/WBMBAlgorithm.java
*/
class TG_WBMBAlgorithm : public NoGo::IPlayer
{
public:
	TG_WBMBAlgorithm();
	~TG_WBMBAlgorithm();

	virtual NoGo::uint MakeMove(const NoGo::Board& currentBoardState) override;


	virtual IPlayer* Clone() override;

private:
	std::vector<NoGo::Coordinate> GetAllValidMoves(const NoGo::Board& board, NoGo::Field color);
	std::chrono::time_point<std::chrono::steady_clock> moveEndTime;

	long long GetRemainingTimeMS();
};

