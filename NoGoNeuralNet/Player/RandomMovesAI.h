#pragma once
#include "IPlayer.h"
namespace NoGo
{
	class RandomMovesAI : public IPlayer
	{
	public:

		virtual uint MakeMove(const Board& currentBoardState) override;
		virtual IPlayer* Clone() override;
	};
}

