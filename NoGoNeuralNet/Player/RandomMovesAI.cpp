#include "RandomMovesAI.h"

namespace NoGo
{

	uint RandomMovesAI::MakeMove(const Board& currentBoardState)
	{
		uint move = (uint)(-1);

		do
		{
			move = rand() % Board::BOARD_SIZE;
		} while (!IsValidMove(move));

		return move;
	}

	IPlayer* RandomMovesAI::Clone()
	{
		return new RandomMovesAI();
	}

}

