#pragma once
#include <vector>
#include "IPlayer.h"

/*
 * Source:
 * https://github.com/lukaszlipski/NoGo/blob/master/app/src/main/java/com/lucek/androidgameengine2d/controllers/AI_KosmaMarzec.java
 */
class TG_AKMMAlgorithm : public NoGo::IPlayer
{
public:
	TG_AKMMAlgorithm();
	~TG_AKMMAlgorithm();

	virtual NoGo::uint MakeMove(const NoGo::Board& currentBoardState) override;
	virtual NoGo::IPlayer* Clone() override;

private:
	void SetupValuesMap();

	int valuesMap[NoGo::Board::BOARD_SIZE];
	int moveValues[NoGo::Board::BOARD_SIZE];
	int simulationCounts[NoGo::Board::BOARD_SIZE];

	std::vector<NoGo::Coordinate> possiblePoints;
	std::vector<NoGo::Coordinate> emptyPoints;
};

