#include "stdafx.h"
#include <iostream>
#include <stdlib.h>
#include <time.h>
#include "Game/NoGoGame.h"
#include "Genetic/GeneticLearning.h"
#include "Graphics/ASCIIPrinter.h"
#include "Neural/NetworkHelpers.h"
#include "Neural/NeuralNetwork.h"
#include "Player/NeuralNetworkPlayer.h"
#include "Player/RandomMovesAI.h"
#include "Player/TG_BGKSAlgorithm.h"
#include "Utility/Algorithms.h"
#include "Network/NetworkGameClient.h"

using namespace NoGo;

void PlaySingleGameSample();
void GeneticLearningSample();
void LoadNetworkSample(); 
void AlgorithmsTest(IPlayer* algo);
void TrainFurther(const char* networkToLoad, IPlayer* against, size_t numberOfEpochs = 20);
void printScores(std::vector<int>& scores);
void NetworkGame(const char* serverIP, const char* servertPort);


int main(int argc, char** argv)
{
	srand((unsigned int)time(0));

	{
		//argv[0] is always path to the executable, so argc is always >=1.
		const char* serverIP = (argc > 1) ? argv[1] : "127.0.0.1";
		const char* serverPort = (argc > 2) ? argv[2] : "6789";
		NetworkGame(serverIP, serverPort);
	}


	//GeneticLearningSample();
	//LoadNetworkSample();
	//AlgorithmsTest(new TG_BGKSAlgorithm());
	//TrainFurther("bestNetwork.nn", new TG_BGKSAlgorithm(), 20);

	std::cout << "\n\nPress any key to close application... ";
	getchar();
    return EXIT_SUCCESS;
}

void NetworkGame(const char* serverIP, const char* servertPort)
{
	Neural::BasicNeuralNetwork* network = Neural::Files::LoadNetworkFromFile<float>("bestNetwork-2017-12-11.nn");
	NeuralNetworkPlayer* networkPlayer = new NeuralNetworkPlayer(network);

	NetworkGameClient game(networkPlayer);
	if (game.Connect(serverIP, servertPort))
	{
		std::cout << "Connected to server successfully. Awaiting first game" << std::endl;
		game.PlayTurney();
		std::cout << "Tourney finished!";
	}
	else
	{
		std::cout << "Unable to connect to server. Aborting." << std::endl;
	}

	delete networkPlayer;
	delete network;
}



void TrainFurther(const char* networkToLoad, IPlayer* against, size_t numberOfEpochs /*= 20*/)
{
	Neural::BasicNeuralNetwork* network = Neural::Files::LoadNetworkFromFile<float>(networkToLoad);

	size_t lsize = Board::BOARD_SIZE;
	//number of neurons in each consecutive layer of our neural nets
	size_t layerSizes[] = { Board::BOARD_SIZE,lsize,lsize,lsize,Board::BOARD_SIZE };

	Genetic::GeneticLearning evolution(layerSizes, 5);
	evolution.populationSize = 20;			//in every generation, 20 networks will compete to be the best
	evolution.gamesPlayedPerScoring = 20;	//their score is determined from playing 20 games against some other AI (defined below)
	evolution.survivalRate = 0.20f;			//only about 20% of networks will get to procreate and have their genes passed on
	evolution.mutationRate = 0.015f;		//to better adapt to environment, each generation will have 1.5% of their genes randomized
	evolution.elitism = true;				//the best individual from previous population gets to survive to the next one
	evolution.numberOfScoringThreads = 20;	//because why the fuck not

	IPlayer* opponentToTrainAgainst = against;	//opponent our NeuralNetworks will train to beat
	evolution.Initialize(opponentToTrainAgainst, network);	//initialize buffers. Above values SHOULD NOT be changed after this point


	do
	{

		std::cout << "Generation: " << evolution.GetCurrentGenerationCounter() << std::endl;
		evolution.NextGeneration();		//score current generation, then let them procreate and evolve

		const int* scores = evolution.GetScores();	//receive scores from PREVIOUS generation
		std::vector<int> networkScores(scores, scores + evolution.populationSize);	//sorting is performed on vector for convenience

																					//display current data
		std::cout << "Generation: " << evolution.GetCurrentGenerationCounter() << std::endl;
		std::cout << "Score from " << evolution.gamesPlayedPerScoring <<
			" games against random AI (win adds +5 to score, lose -10 and tie +1):" << std::endl << std::endl;

		printScores(networkScores);

		//getchar();
		//system("cls");
		std::cout << "\n-----\n";

	} while (evolution.GetCurrentGenerationCounter() < numberOfEpochs);

	std::cout << "Final scores (generation " << evolution.GetCurrentGenerationCounter() << "):" << std::endl;
	{
		evolution.ScoreGeneration();	//force scoring current generation. Usually it happens automatically
										//when NextGeneration() gets called
		const int* scores = evolution.GetScores();
		std::vector<int> networkScores(scores, scores + evolution.populationSize);
		printScores(networkScores);
	}
	getchar();

	Neural::Files::SaveNetworkToFile(&evolution.GetBest(), "bestNetwork2.nn");


	delete network;
}

void AlgorithmsTest(IPlayer* algo)
{
	size_t layerSizes[] = { Board::BOARD_SIZE,Board::BOARD_SIZE,Board::BOARD_SIZE };
	Neural::BasicNeuralNetwork network(layerSizes, 3);
	Neural::HelperFunctions::RandomizeNeuralNetworkTresholds(&network, 0.0f, 1.0f);
	Neural::HelperFunctions::RandomizeNeuralNetworkWeights(&network, -2.0f, 2.0f);
	IPlayer* player0 = new NeuralNetworkPlayer(&network);
	IPlayer* player1 = algo;


#define DEBUG_GAME Graphics::PrintBoard(game.board); std::cout << std::endl; getchar()
	NoGoGame game(player0, player1);

	MoveResult result = MoveResult::GAME_CONTINUES;
	while (result == MoveResult::GAME_CONTINUES)
	{
		result = game.Update();
	}

	switch (result)
	{
	case NoGo::MoveResult::BLACK_WON:
		std::cout << "Player BLACK won!" << std::endl;
		break;
	case NoGo::MoveResult::WHITE_WON:
		std::cout << "Player WHITE won!" << std::endl;
		break;
	default:
		std::cerr << "ERROR!" << std::endl;
		break;
	}

	std::cout << std::endl;
	DEBUG_GAME;


	delete player0;
	delete player1;
}

void PlaySingleGameSample()
{
	size_t layerSizes[] = { Board::BOARD_SIZE,Board::BOARD_SIZE,Board::BOARD_SIZE };
	Neural::BasicNeuralNetwork network(layerSizes, 3);
	Neural::HelperFunctions::RandomizeNeuralNetworkTresholds(&network, 0.0f, 1.0f);
	Neural::HelperFunctions::RandomizeNeuralNetworkWeights(&network, -2.0f, 2.0f);
	IPlayer* player0 = new NeuralNetworkPlayer(&network);
	IPlayer* player1 = new RandomMovesAI();


#define DEBUG_GAME Graphics::PrintBoard(game.board); std::cout << std::endl; getchar()
	NoGoGame game(player0, player1);

	MoveResult result = MoveResult::GAME_CONTINUES;
	while (result == MoveResult::GAME_CONTINUES)
	{
		result = game.Update();
	}

	switch (result)
	{
	case NoGo::MoveResult::BLACK_WON:
		std::cout << "Player BLACK won!" << std::endl;
		break;
	case NoGo::MoveResult::WHITE_WON:
		std::cout << "Player WHITE won!" << std::endl;
		break;
	default:
		std::cerr << "ERROR!" << std::endl;
		break;
	}

	std::cout << std::endl;
	DEBUG_GAME;


	delete player0;
	delete player1;

}

void GeneticLearningSample()
{
	size_t lsize = Board::BOARD_SIZE;
	//number of neurons in each consecutive layer of our neural nets
	size_t layerSizes[] = { Board::BOARD_SIZE,lsize,lsize,lsize,lsize,lsize,lsize,Board::BOARD_SIZE };

	Genetic::GeneticLearning evolution(layerSizes, 8);
	evolution.populationSize = 40;			//in every generation, 20 networks will compete to be the best
	evolution.gamesPlayedPerScoring = 20;	//their score is determined from playing 20 games against some other AI (defined below)
	evolution.survivalRate = 0.20f;			//only about 20% of networks will get to procreate and have their genes passed on
	evolution.mutationRate = 0.015f;		//to better adapt to environment, each generation will have 1.5% of their genes randomized
	evolution.elitism = true;				//the best individual from previous population gets to survive to the next one
	evolution.numberOfScoringThreads = 20;	//because why the fuck not

	IPlayer* opponentToTrainAgainst = new RandomMovesAI();	//opponent our NeuralNetworks will train to beat
	evolution.Initialize(opponentToTrainAgainst);			//initialize buffers. Above values SHOULD NOT be changed after this point

	std::cout << "For how many generations should we be training networks?";
	size_t maxNumGenerations;
	std::cin >> maxNumGenerations;


 	do
 	{

		std::cout << "Generation: " << evolution.GetCurrentGenerationCounter() << std::endl;
		evolution.NextGeneration();		//score current generation, then let them procreate and evolve

		const int* scores = evolution.GetScores();	//receive scores from PREVIOUS generation
		std::vector<int> networkScores(scores, scores + evolution.populationSize);	//sorting is performed on vector for convenience

																					//display current data
		std::cout << "Generation: " << evolution.GetCurrentGenerationCounter() << std::endl;
		std::cout << "Score from " << evolution.gamesPlayedPerScoring <<
			" games against random AI (win adds +5 to score, lose -10 and tie +1):" << std::endl << std::endl;

		printScores(networkScores);

		//getchar();
		//system("cls");
		std::cout << "\n-----\n";

	} while (evolution.GetCurrentGenerationCounter() < maxNumGenerations);

	std::cout << "Final scores (generation " << evolution.GetCurrentGenerationCounter() << "):" << std::endl;
	{
		evolution.ScoreGeneration();	//force scoring current generation. Usually it happens automatically
										//when NextGeneration() gets called
		const int* scores = evolution.GetScores();
		std::vector<int> networkScores(scores, scores + evolution.populationSize);
		printScores(networkScores);
	}
	getchar();

	Neural::Files::SaveNetworkToFile(&evolution.GetBest(), "bestNetwork-2017-12-11.nn");
}

void LoadNetworkSample()
{
	Neural::BasicNeuralNetwork* network = Neural::Files::LoadNetworkFromFile<float>("bestNetwork.nn");

	NeuralNetworkPlayer networkPlayer(network);
	RandomMovesAI opponent;

	{
		NoGoGame game(&networkPlayer, &opponent);

		MoveResult result = MoveResult::GAME_CONTINUES;
		while (result == MoveResult::GAME_CONTINUES)
		{
			result = game.Update();
			DEBUG_GAME;
		}

		switch (result)
		{
		case NoGo::MoveResult::BLACK_WON:
			std::cout << "Network won!" << std::endl;
			break;
		case NoGo::MoveResult::WHITE_WON:
			std::cout << "Random AI won!" << std::endl;
			break;
		default:
			std::cerr << "ERROR!" << std::endl;
			break;
		}

		std::cout << std::endl;
		DEBUG_GAME;
	}
	{
		NoGoGame game(&opponent, &networkPlayer);

		MoveResult result = MoveResult::GAME_CONTINUES;
		while (result == MoveResult::GAME_CONTINUES)
		{
			result = game.Update();
			DEBUG_GAME;
		}

		switch (result)
		{
		case NoGo::MoveResult::BLACK_WON:
			std::cout << "Random AI won!" << std::endl;
			break;
		case NoGo::MoveResult::WHITE_WON:
			std::cout << "Network won!" << std::endl;
			break;
		default:
			std::cerr << "ERROR!" << std::endl;
			break;
		}

		std::cout << std::endl;
		DEBUG_GAME;
	}
	
}




float mean(const std::vector<int>& numbers)
{
	float result = 0.0f;
	for (int i : numbers)
	{
		result += (float)i;
	}
	return result / (float)numbers.size();
}

void printScores(std::vector<int>& scores)
{
	std::vector<size_t> sortedFromWorst = sort_indexes(scores);	//sort them, so they'll look cleaner when displayed

	for (int i = sortedFromWorst.size() - 1; i >= 0; --i)
	{
		std::cout << scores[sortedFromWorst[i]] << '\t';
	}

	std::cout << std::endl << std::endl << "Mean score: " << mean(scores) << std::endl;
}


