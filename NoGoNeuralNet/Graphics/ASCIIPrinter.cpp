#include "ASCIIPrinter.h"

namespace Graphics
{

	void PrintBoard(const NoGo::Board& board)
	{
		std::cout << board << std::endl;
	}

	std::ostream& operator<<(std::ostream& stream, const NoGo::Board& board)
	{
		using namespace NoGo;
		const char* characters = ".XO#";
		Coordinate coords;
		for (size_t y = 0; y < Board::SIZE_Y; y++)
		{
			coords.y = (NoGo::uint)y;
			for (size_t x = 0; x < Board::SIZE_X; x++)
			{
				coords.x = (NoGo::uint)x;

				stream << characters[(size_t)board.Get(coords)];
			}
			stream << std::endl;
		}

		return stream;
	}

}
