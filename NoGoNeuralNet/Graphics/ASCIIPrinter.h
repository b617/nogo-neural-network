#pragma once
#include <iostream>
#include "../Game/Board.h"

namespace Graphics
{
	void PrintBoard(const NoGo::Board& board);
	std::ostream& operator<<(std::ostream& stream, const NoGo::Board& board);
}

