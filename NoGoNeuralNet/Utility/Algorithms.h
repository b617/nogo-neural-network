#pragma once
#include <algorithm>
#include <numeric>
#include <vector>

/**
* @brief Sorts an array and returns array of indexes for sorted version
* 
* Copied from StackOverflow, lol
* Source:
* https://stackoverflow.com/questions/1577475/c-sorting-and-keeping-track-of-indexes
* @param Array to sort
* @return Array of indexes representing sorted values from original array
*/
template <typename T>
std::vector<size_t> sort_indexes(const std::vector<T> &v) 
{
	// initialize original index locations
	std::vector<size_t> idx(v.size());
	iota(idx.begin(), idx.end(), 0);

	// sort indexes based on comparing values in v
	sort(idx.begin(), idx.end(),
		[&v](size_t i1, size_t i2) {return v[i1] < v[i2]; });

	return idx;
}

/**
* @brief Copied from here:
* https://stackoverflow.com/questions/10366327/dividing-a-integer-equally-in-x-parts
*/
static std::vector<size_t> divide_integer_evenly(const size_t integerToDivide, const size_t divideBetween)
{
	std::vector<size_t> ints(divideBetween);

	const size_t intDivision = integerToDivide / divideBetween;
	for (size_t i = 0; i < divideBetween; i++)
	{
		ints[i] = intDivision;
	}

	for (size_t i = 0; i < integerToDivide%divideBetween; i++)
	{
		ints[i] += 1;
	}

	return ints;
}