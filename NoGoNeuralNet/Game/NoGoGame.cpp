#include "NoGoGame.h"
#include <iostream>
#include <vector>
#include "Group.h"
#include "../Player/IPlayer.h"

namespace NoGo
{
	NoGoGame::NoGoGame(class IPlayer* playerBlack, class IPlayer* playerWhite, bool deletePlayersOnDtor /*= false*/)
		:playerBlack(playerBlack), playerWhite(playerWhite), deletePlayersOnDtor(deletePlayersOnDtor)
	{
		/*playerBlack->currentGame = playerWhite->currentGame = this;
		playerBlack->color = Field::BLACK;	playerWhite->color = Field::WHITE;*/
		SetCurrentGame(playerBlack, Field::BLACK);
		SetCurrentGame(playerWhite, Field::WHITE);
		currentTurn = Field::BLACK;
	}

	NoGoGame::NoGoGame()
	{
		deletePlayersOnDtor = false;
	}

	NoGoGame::~NoGoGame()
	{
		if (deletePlayersOnDtor)
		{
			delete playerBlack;
			delete playerWhite;
		}
	}

	bool NoGoGame::IsValidMove(Coordinate coords)
	{
		return IsValidMove_internal(coords.ToFieldIndex(), currentTurn);
	}
	bool NoGoGame::IsValidMove(uint index)
	{
		return IsValidMove_internal(index, currentTurn);
	}
	bool NoGoGame::IsValidMove(Coordinate coords, Field color)
	{
		return IsValidMove_internal(coords.ToFieldIndex(), color);
	}
	bool NoGoGame::IsValidMove_internal(uint index, Field color)
	{
		if (index >= Board::BOARD_SIZE) return false;
		if (board.Get(index) != Field::EMPTY) return false;

		Board boardCopy = board;
		boardCopy.MakeMove(index, color);
		return Group::DoAllGroupsHaveLiberties(boardCopy);
	}

	bool NoGoGame::IsValidMove(const Board& board, const uint move, const Field color)
	{
		if (move >= Board::BOARD_SIZE) return false;
		if (board.Get(move) != Field::EMPTY) return false;

		Board boardCopy = board;
		boardCopy.MakeMove(move, color);
		return Group::DoAllGroupsHaveLiberties(boardCopy);
	}

	MoveResult NoGoGame::Update()
	{
		IPlayer* currentPlayer = GetCurrentPlayer();
		auto move = currentPlayer->MakeMove(board);
		if (!IsValidMove(move))
		{
			//returning wrong move equals auto-losing
			std::cerr << "Player tried to make an illegal move: " << (int)move << std::endl;
			return PlayerWon(OppositeColor(currentPlayer->color));
		}
		MakeMove(move);
		return GetBoardStateResult();
	}
	MoveResult NoGoGame::GetBoardStateResult()
	{
		//Check if any move can be made
		for (uint i = 0; i < Board::BOARD_SIZE; i++)
		{
			if (board.Get(i) == Field::EMPTY)
			{
				//small optimization: if it doesn't have any neighbors, it can't be a problem:
				std::vector<uint> neighbors = Board::GetNeighbors(i);
				bool allNeighborsAreEmpty = true;
				for (uint neighbor : neighbors)
				{
					if (board.Get(neighbor) != Field::EMPTY)
					{
						allNeighborsAreEmpty = false;
						break;
					}
				}
				if (allNeighborsAreEmpty)
				{
					return MoveResult::GAME_CONTINUES;
				}

				//gotta check manually. Run the simulation, Scott!
				if (IsValidMove(i))
				{
					return MoveResult::GAME_CONTINUES;
				}
			}
		}

		//no move can be made.
		return PlayerWon(OppositeColor(currentTurn));
	}

	void NoGoGame::MakeMove(Coordinate coords)
	{
		MakeMove(coords.ToFieldIndex());
	}
	void NoGoGame::MakeMove(uint index)
	{
		board.MakeMove(index, currentTurn);
		SwitchTurns();
	}

	void NoGoGame::SwitchTurns()
	{
		currentTurn = OppositeColor(currentTurn);
	}

	IPlayer* NoGoGame::GetCurrentPlayer()
	{
		return (currentTurn == Field::BLACK) ? playerBlack : playerWhite;
	}

	void NoGoGame::SetCurrentGame(IPlayer* player, Field color)
	{
		player->currentGame = this;
		player->color = color;
	}

}
