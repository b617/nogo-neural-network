#pragma once
#include "Board.h"

namespace NoGo
{
	class NoGoGame
	{
	public:
		class IPlayer* playerBlack, *playerWhite;
		Board board;
		Field currentTurn = Field::BLACK;
	private:
		bool deletePlayersOnDtor;
	public:

		NoGoGame(class IPlayer* playerBlack, class IPlayer* playerWhite, bool deletePlayersOnDtor = false);
	protected:
		NoGoGame();
	public:
		~NoGoGame();

		inline bool IsValidMove(Coordinate coords);
		inline bool IsValidMove(uint index);
		inline bool IsValidMove(Coordinate coords, Field color);
		bool IsValidMove(const Board& board, const uint move, const Field color);
		bool IsValidMove_internal(uint index, Field color);

		virtual MoveResult Update();
		MoveResult GetBoardStateResult();

		inline void MakeMove(Coordinate coords);
		void MakeMove(uint index);

	protected:
		void SwitchTurns();
		inline class IPlayer* GetCurrentPlayer();
		void SetCurrentGame(IPlayer* player, Field color);
	};
}

