#include "Group.h"
#include <stack>
#include <unordered_set>

namespace NoGo
{
	std::vector<Group> Group::GetAllGroups(const Board& board)
	{
		bool visited[Board::BOARD_SIZE];
		memset(visited, 0, sizeof(bool)*Board::BOARD_SIZE);
		std::vector<Group> groups;

		for (uint i = 0; i < Board::BOARD_SIZE; i++)
		{
			if (visited[i])
			{
				continue;
			}

			Group group;
			std::stack<uint> toVisit;
			std::unordered_set<uint> liberties;
			toVisit.push(i);
			visited[i] = true;
			group.color = board.Get(i);

			if (group.color == Field::EMPTY)
			{
				//we're not interested in groups of empty fields
				continue;
			}

			while (toVisit.size() > 0)
			{
				//process current coordinate
				uint current = toVisit.top();
				toVisit.pop();
				group.groupPoints.push_back(current);

				//process its neighbors
				std::vector<uint> neighbors = Board::GetNeighbors(current);
				for (uint neighbor : neighbors)
				{
					const Field neighborColor = board.Get(neighbor);
					if (neighborColor == Field::EMPTY)
					{
						liberties.insert(neighbor);
					}

					else if (!visited[neighbor] && neighborColor == group.color)
					{
						visited[neighbor] = true;
						toVisit.push(neighbor);
					}
				}

			}

			group.liberties = std::vector<uint>(liberties.begin(), liberties.end());
			groups.push_back(group);
		}

		return groups;
	}

	Group Group::GetGroupAtPoint(uint pointIndex, const Board& board)
	{
		bool visited[Board::BOARD_SIZE];
		memset(visited, 0, sizeof(bool)*Board::BOARD_SIZE);
		std::stack<uint> toVisit;
		std::unordered_set<uint> liberties;
		Group group;
		group.color = board.Get(pointIndex);
		toVisit.push(pointIndex);
		visited[pointIndex] = true;

		while (toVisit.size() > 0)
		{
			//process current coordinate
			uint current = toVisit.top();
			toVisit.pop();
			group.groupPoints.push_back(current);

			//process its neighbors
			std::vector<uint> neighbors = Board::GetNeighbors(current);
			for (uint neighbor : neighbors)
			{
				const Field neighborColor = board.Get(neighbor);
				if (neighborColor == Field::EMPTY)
				{
					liberties.insert(neighbor);
				}

				else if (!visited[neighbor] && neighborColor == group.color)
				{
					visited[neighbor] = true;
					toVisit.push(neighbor);
				}
			}

		}

		group.liberties = std::vector<uint>(liberties.begin(), liberties.end());
		return group;
	}

	bool Group::DoAllGroupsHaveLiberties(const Board& board)
	{
		bool visited[Board::BOARD_SIZE];
		memset(visited, 0, sizeof(bool)*Board::BOARD_SIZE);

		for (uint i = 0; i < Board::BOARD_SIZE; i++)
		{
			if (visited[i])
			{
				continue;
			}

			std::stack<uint> toVisit;
			std::unordered_set<uint> liberties;
			toVisit.push(i);
			visited[i] = true;
			const Field groupColor = board.Get(i);
			if (groupColor == Field::EMPTY)
			{
				//we're not interested in groups of empty fields
				continue;
			}

			while (toVisit.size() > 0)
			{
				//process current coordinate
				uint current = toVisit.top();
				toVisit.pop();

				//process its neighbors
				std::vector<uint> neighbors = Board::GetNeighbors(current);
				for (uint neighbor : neighbors)
				{
					const Field neighborColor = board.Get(neighbor);
					if (neighborColor == Field::EMPTY)
					{
						liberties.insert(neighbor);
					}

					else if (!visited[neighbor] && neighborColor == groupColor)
					{
						visited[neighbor] = true;
						toVisit.push(neighbor);
					}
				}

			}

			if (liberties.size() <= 0)
			{
				return false;
			}
		}

		return true;
	}

}
