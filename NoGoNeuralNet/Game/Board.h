#pragma once
#include <vector>

namespace NoGo
{
	typedef unsigned char uint;

	enum class Field : unsigned char
	{
		EMPTY = 0,
		BLACK = 1,
		WHITE = 2
	};
	Field OppositeColor(Field color);

	enum class MoveResult : unsigned char
	{
		GAME_CONTINUES = 0,
		BLACK_WON = 1,
		WHITE_WON = 2,
		TIE
	};
	MoveResult PlayerWon(Field player);

	struct Coordinate;

	class Board
	{
	public:
		static const size_t SIZE_X = 9;
		static const size_t SIZE_Y = 9;
		static const size_t BOARD_SIZE = SIZE_X * SIZE_Y;

		Field data[BOARD_SIZE];

		Board();
		Board(const Board& source);
		Board operator=(const Board& source);
		~Board();

		void MakeMove(Coordinate coords, Field color);
		void MakeMove(uint index, Field color);

		void Reset();

		Field Get(Coordinate coords) const;
		Field Get(uint index) const;

		static std::vector<uint> GetNeighbors(uint index);
	};

	struct Coordinate
	{
	private:
		typedef unsigned char coordinate_type;
	public:
		coordinate_type x, y;

		Coordinate() : x(0), y(0)
		{ }
		Coordinate(coordinate_type x, coordinate_type y) : x(x), y(y)
		{ }

		inline static Coordinate FromFieldIndex(uint index)
		{
			return Coordinate(index % Board::SIZE_X, index / Board::SIZE_Y);
		}
		inline uint ToFieldIndex() const
		{
			return (y * Board::SIZE_X) + x;
		}
		operator uint() const
		{
			return ToFieldIndex();
		}
	};
}


