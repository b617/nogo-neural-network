#include "Board.h"
#include <string>

namespace NoGo
{
	Board::Board()
	{
		memset(data, 0, sizeof(Field)*BOARD_SIZE);
	}

	Board::Board(const Board& source)
	{
		memcpy(data, source.data, sizeof(Field)*BOARD_SIZE);
	}

	NoGo::Board Board::operator=(const Board& source)
	{
		if (&source != this)
		{
			memcpy(data, source.data, sizeof(Field)*BOARD_SIZE);
		}
		return *this;
	}

	Board::~Board()
	{
	}

	void Board::MakeMove(Coordinate coords, Field color)
	{
		data[coords.ToFieldIndex()] = color;
	}
	void Board::MakeMove(uint index, Field color)
	{
		data[index] = color;
	}

	void Board::Reset()
	{
		memset(data, 0, BOARD_SIZE);
	}

	Field Board::Get(Coordinate coords) const
	{
		return data[coords.ToFieldIndex()];
	}
	Field Board::Get(uint index) const
	{
		return data[index];
	}

	std::vector<uint> Board::GetNeighbors(uint index)
	{
		std::vector<uint> neighbors;
#define PUSH_COORDS(X,Y) neighbors.push_back(Coordinate(X,Y).ToFieldIndex())
		Coordinate coordsCurrent = Coordinate::FromFieldIndex(index);
		if (coordsCurrent.x > 0) PUSH_COORDS(coordsCurrent.x - 1, coordsCurrent.y);
		if (coordsCurrent.x < Board::SIZE_X - 1) PUSH_COORDS(coordsCurrent.x + 1, coordsCurrent.y);
		if (coordsCurrent.y > 0) PUSH_COORDS(coordsCurrent.x, coordsCurrent.y - 1);
		if (coordsCurrent.y < Board::SIZE_Y - 1) PUSH_COORDS(coordsCurrent.x, coordsCurrent.y + 1);
#undef PUSH_COORDS
		return neighbors;
	}

	Field OppositeColor(Field color)
	{
		switch (color)
		{
		case Field::BLACK:
			return Field::WHITE;
		case Field::WHITE:
			return Field::BLACK;
		default:
			return Field::EMPTY;
		}
	}

	MoveResult PlayerWon(Field player)
	{
		return (MoveResult)player;
	}

}
