#pragma once
#include "Board.h"
#include <vector>

namespace NoGo
{

	struct Group
	{
	public:
		std::vector<uint> groupPoints;
		std::vector<uint> liberties;
		Field color;

		static std::vector<Group> GetAllGroups(const Board& board);
		static Group GetGroupAtPoint(uint pointIndex, const Board& board);
		inline static Group GetGroupAtPoint(Coordinate coords, const Board& board)
		{
			return GetGroupAtPoint(coords.ToFieldIndex(), board);
		}

		static bool DoAllGroupsHaveLiberties(const Board& board);
	};
}

