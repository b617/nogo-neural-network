/* Klient grajacy losowo w Toads and Frogs. Do komunikacji sieciowej
wykorzystano bibliotekę Asio z ogólnodostępnego zestawu bibliotek Boost
(http://www.boost.org/). Kompilacja w linuksie:

g++ -I/path/to/include rplayer.cpp -o rplayer -L/path/to/lib -lboost_system -lboost_thread -pthread

Uruchamianie:

rplayer nazwa_wlasna adres_ip_serwera port

Autor: wojciech.wieczorek@us.edu.pl
*/
#include <iostream>
#include <utility>
#include <cstdlib>
#include <sstream>
#include <string>
#include <vector>
#include <ctime>
#include <boost/optional.hpp>
#include <boost/asio.hpp>

using boost::asio::ip::tcp;

typedef std::pair<int, int> ruch;

boost::optional<ruch> losuj_ruch(std::string &p, int n, char k) {
  std::vector<ruch> opcje;
  int i;
  if (k == 'T') {
    for(i=0; i<=n-3; i++) {
      if (p[i] == 'T' && p[i+1] == '.')
        opcje.push_back(std::make_pair(i, i+1));
      if (p[i] == 'T' && p[i+1] == 'F' && p[i+2] == '.')
        opcje.push_back(std::make_pair(i, i+2));
    }
    if (p[n-2] == 'T' && p[n-1] == '.')
      opcje.push_back(std::make_pair(n-2, n-1));
  }
  else {  // kolor == 'F'
    for(i=n-1; i>=2; i--) {
      if (p[i] == 'F' && p[i-1] == '.')
        opcje.push_back(std::make_pair(i, i-1));
      if (p[i] == 'F' && p[i-1] == 'T' && p[i-2] == '.')
        opcje.push_back(std::make_pair(i, i-2));
    }
    if (p[1] == 'F' && p[0] == '.')
      opcje.push_back(std::make_pair(1, 0));
  }
  if (opcje.size() == 0)
    return boost::optional<ruch>();
  ruch wybrany = opcje[std::rand() % opcje.size()];
  p[wybrany.first] = '.';
  p[wybrany.second] = k;
  return boost::optional<ruch>(wybrany);
}

int main(int argc, char* argv[]) {
  if (argc != 4) {
    std::cerr << "Uzycie: rplayer nazwa_wlasna adres_ip_serwera port" << std::endl;
    return 1;
  }
  std::srand(std::time(0));  // inicjacja generatora liczb pseudolosowych
  try {
    tcp::iostream serwer(argv[2], argv[3]);  // proba nawiazania polaczenia z serwerem
    if (!serwer) {
      std::cout << "Polaczenie z serwerem nieudane." << std::endl;
      return 1;
    }
    std::string nazwa_wlasna(argv[1]);
    serwer << nazwa_wlasna << "\r\n";
    std::cout << "Wyslalem: " << nazwa_wlasna << std::endl;
    char kolor;
    int czas, dl_planszy;
    std::string komunikat, plansza;
    std::istringstream ins;
    std::getline(serwer, komunikat);
    ins.clear();
    ins.str(komunikat);
    ins >> kolor >> czas >> dl_planszy >> plansza;
    std::cout << "Odebralem: " << kolor << " " << czas << " " << dl_planszy << " " << plansza << std::endl;
    char kp = (kolor == 'T' ? 'F' : 'T');  // kolor przeciwnika
    if (kolor == 'F') {  // gdy rozpoczynam
      boost::optional<ruch> r = losuj_ruch(plansza, dl_planszy, kolor);
      std::cout << plansza << std::endl;
      if (r) {
        serwer << (*r).first << " " << (*r).second << "\r\n";
        std::cout << "Wyslalem: " << (*r).first << " " << (*r).second << std::endl;
      }
      else {
        std::cout << "Przegralem" << std::endl;
        return 0;
      }
    }
    std::getline(serwer, komunikat);
    std::cout << "Odebralem: " << komunikat << std::endl;
    while (komunikat[0] != 'E') {  // nie END
      int a, b;
      ins.clear();
      ins.str(komunikat);
      ins >> a >> b;
      plansza[a] = '.';
      plansza[b] = kp;
      std::cout << plansza << std::endl;
      boost::optional<ruch> r = losuj_ruch(plansza, dl_planszy, kolor);
      if (r) {
        std::cout << plansza << std::endl;
        serwer << (*r).first << " " << (*r).second << "\r\n";
        std::cout << "Wyslalem: " << (*r).first << " " << (*r).second << std::endl;
        std::getline(serwer, komunikat);
        std::cout << "Odebralem: " << komunikat << std::endl;
      }
      else {
        std::getline(serwer, komunikat);
        std::cout << "Odebralem: " << komunikat << std::endl;
        std::cout << "Przegralem" << std::endl;
        return 0;
      }
    }
    std::cout << "Wygralem" << std::endl;
  }
  catch (std::exception& e) {
    std::cout << "Wyjatek: " << e.what() << std::endl;
  }
  return 0;
}

